const readline = require('readline');
const onChange = require('on-change');
const util = require('util');
const tmi = require('tmi.js');
const fs = require('fs');

const CONFIG_FILE = './config.json';
const LANGUAGE_FILE = './lang.json';
const PLUGINS_DIR = './plugins/';
const RETURN_CODES = {
  OK: 0,
  INPUT_ERROR: 1,
  CONNECTION_ERROR: 2,
  PLUGIN_ERROR: 3
};

class ConfigLoader {
  constructor(configFile, autosave = true) {
    this._configFile = configFile;
    this._config = JSON.parse(fs.readFileSync(this._configFile));
    
    if (autosave) {
      this._changed = false;
      this._watchedConfig = onChange(this._config, () => {
        this._changed = true;
      });
      
      setInterval(() => {
        if (this._changed) {
          this._changed = false;
          this.save();
        }
      }, 5000);
    }
  }

  get() {
    return this._watchedConfig;
  }

  save() {
    fs.writeFile(this._configFile, JSON.stringify(this._config, null, 2), () => {});
  }
}

var configLoader = new ConfigLoader(CONFIG_FILE);
var config = configLoader.get();

class Localization {
  constructor(langsFile) {
    this.langs = JSON.parse(fs.readFileSync(langsFile));
    this.curLang = 'RU';
  }

  getRaw(id, undef = 'no message... ') {
    if (typeof id == 'string') {
      id = id.split('.').map(x => x.trim());
    }

    let ptr = this.langs[this.curLang];
    for (let key of id) {
      ptr = ptr[key];
      if (!ptr) {
        return undef;
      }
      if (typeof ptr == 'string') {
        return ptr;
      }
    }
    if (typeof ptr == 'object') {
      if (ptr['.'] != undefined) {
        return ptr['.'];
      } else {
        return undef;
      }
    }
    return ptr;
  }

  getMsg(id, ...params) {
    params = params.map((x) => {
      return x.toString ? x.toString() : x;
    });
    return util.format(this.getRaw(id), ...params);
  }
}

var lang = new Localization(LANGUAGE_FILE);

const TR = lang.getMsg.bind(lang);

class Logger {
  generatePathStr(path) {
    if (!path)
      return '';

    let pathArr;
    if (typeof path == 'string') {
      pathArr = [path];
    } else {
      pathArr = path;
    }

    return ' ' + pathArr.join('/');
  }

  log(path = false) {
    return (...text) => {
      console.log(`[${TR('logger.info')}${this.generatePathStr(path)}] `, ...text);
    }
  }

  warn(path = false) {
    return (...text) => {
      console.log(`[${TR('logger.warning')}${this.generatePathStr(path)}] `, ...text);
    }
  }

  error(path = false) {
    return (...text) => {
      console.log(`[${TR('logger.error')}${this.generatePathStr(path)}] `, ...text);
    }
  }
}

const logger = new Logger();

class PluginManager {
  constructor(plugins_dir) {
    this.plugins = {};
    for (let pluginFile of fs.readdirSync(plugins_dir)) {
      if (pluginFile.match(/\.js$/)) {
        let plugin = require(plugins_dir + pluginFile.replace(/\.[^/.]+$/, ""));
        this.plugins[pluginFile] = new plugin.Plugin();
      }
    }
  }

  makeConfig(config) {
    for (let plName in this.plugins) {
      try {
        if (!config[plName]) {
          config[plName] = {
            config: {}
          };
        }

        config[plName].config = this.plugins[plName].makeConfig( config[plName].config );
      } catch (e) {
        logger.error('plugin loader')('Exception while loading plugin: ', e);
        process.exit(RETURN_CODES.PLUGIN_ERROR);
      }
    }
  }

  init(tmiClient, config) {
    for (let plName in this.plugins) {
      try {
        this.plugins[plName].init(
          tmiClient, 
          config[plName].config
        );
      } catch (e) {
        logger.error('plugin loader')('Exception while loading plugin: ', e);
        process.exit(RETURN_CODES.PLUGIN_ERROR);
      }
    }
  }
}

class Bot {
  constructor() {
    (async () => {
      this.createReadLine();
      this.plMng = new PluginManager(PLUGINS_DIR);
      await this.createConfig();
      this.plMng.makeConfig(config.plugins);
      this.createClient();
      this.plMng.init(this.client, config.plugins);
    })()
  }

  createReadLine() {
    this.rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
      terminal: false
    });
    this.rl.questionSync = function (text) {
      return new Promise((resolve) => this.question(text, resolve));
    };
  }

  createClient() {
    this.client = new tmi.client({
      identity: {
        username: config.username,
        password: config.token
      },
      channels: config.channels
    });
    this.client.on('connected', () => {
      logger.log('tmi.js')(TR('bot.connected'));
    });
    this.client.connect()
      .catch((err) => {
        logger.error('tmi.js')(TR('bot.connection_error'), err);
        process.exit(RETURN_CODES.CONNECTION_ERROR);
      });
  }

  async createConfig() {
    try {
      while (!config.username) {
        config.username = await this.rl.questionSync(TR('config.enter_nick'));
      }
      while (!config.token) {
        config.token = await this.rl.questionSync(TR('config.enter_token'));
      }
      if (!config.channels) {
        let tmp = (await this.rl.questionSync(TR('config.enter_channels')) || '').split(',').map(x => x.trim().toLowerCase());
        config.channels = [];
        for (let cnl of tmp) {
          if (cnl.match(/^[a-z0-9_]+$/)) {
            config.channels.push(cnl);
          } else {
            logger.warn(TR('config'))(TR('config.invalid_channel'), cnl);
          }
        }
      }
    } catch (e) {
      logger.error(TR('config'))(TR('config.input_error'), e);
      process.exit(RETURN_CODES.INPUT_ERROR);
    }
  }
}

new Bot();